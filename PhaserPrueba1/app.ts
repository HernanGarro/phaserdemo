﻿module Game {
    export class PhaserPrueba1 {
        game: Phaser.Game;
        map;

        constructor() {
            this.game = new Phaser.Game(1280, 720, Phaser.AUTO, 'content', {
                create: this.create, preload: this.preload
            });
        }

        preload() {
            
        }

        create() {
            this.map = this.game.add.tilemap();


        }
    }
}

window.onload = () => {
    var game = new Game.PhaserPrueba1();
}