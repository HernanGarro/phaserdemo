var Game;
(function (Game) {
    var PhaserPrueba1 = (function () {
        function PhaserPrueba1() {
            this.game = new Phaser.Game(1280, 720, Phaser.AUTO, 'content', {
                create: this.create, preload: this.preload
            });
        }
        PhaserPrueba1.prototype.preload = function () {
        };
        PhaserPrueba1.prototype.create = function () {
            this.map = this.game.add.tilemap();
        };
        return PhaserPrueba1;
    }());
    Game.PhaserPrueba1 = PhaserPrueba1;
})(Game || (Game = {}));
window.onload = function () {
    var game = new Game.PhaserPrueba1();
};
//# sourceMappingURL=app.js.map